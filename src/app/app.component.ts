import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';
import { LoggerService } from './services/logger.service';


@Component({
  selector: 'app-root',
  template: `
  <ng2-slim-loading-bar color="blue"></ng2-slim-loading-bar>
  <router-outlet></router-outlet>
  `  
})
export class AppComponent {
  
  constructor(
    public translate: TranslateService,
    private logger: LoggerService
  ) {

    translate.addLangs(environment.langs);
    translate.setDefaultLang(environment.defaultLang);

    let browserLang:string = translate.getBrowserLang();
    if (browserLang) {
      browserLang = browserLang.toLowerCase();
    }
    this.logger.debug("browserLang=" + browserLang);
    translate.use(environment.langs.includes(browserLang) ? browserLang : environment.defaultLang);
  }

}
