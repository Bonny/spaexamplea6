import { Component, OnInit } from '@angular/core';
import { LoggerService } from '../../services/logger.service';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormGroup, FormControl,/*, FormsModule, ReactiveFormsModule*/
  Validators
} from '@angular/forms';
import { AuthenticationService } from '../../services/rest-api.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  success: boolean;
  loading: boolean;
  fail: boolean;
  failMsg: string;

  loginForm: FormGroup;
  username: FormControl;
  password: FormControl;

  returnUrl: string;

  constructor(
    private logger: LoggerService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.logger.debug("LoginComponent.ngOnInit");
    this.loading = this.fail = this.success = false;
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
    this.username = new FormControl('', Validators.required);
    this.password = new FormControl('', Validators.required);
    this.loginForm = new FormGroup({
      username: this.username,
      password: this.password
    });
  }

  private clear(): void {
    this.loginForm.reset();
  }

  login() {
    this.logger.debug("login()");
    this.loading = true;
    this.authenticationService.login(this.username.value, this.password.value)
      .subscribe(
        data => {
          this.logger.debug("login success");
          this.loading = this.fail = false;
          this.success = true;
          this.clear();
          setTimeout(() => {
            this.logger.debug("navigate to returnUrl=" + this.returnUrl);
            this.router.navigate([this.returnUrl]);
          }, 500);
        },
        err => {
          this.logger.warn("login fail");
          this.loading = this.success = false;
          this.fail = true;
          var error = err.error;
          this.failMsg = error.reason;
        });
  }

}
