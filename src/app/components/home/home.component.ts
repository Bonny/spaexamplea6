import { Component, OnInit } from '@angular/core';
import { LoggerService } from '../../services/logger.service';
import { AuthenticationService } from '../../services/rest-api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  constructor(
    private logger: LoggerService
  ) {
  }

  ngOnInit() {
    this.logger.debug("HomeComponent.ngOnInit");
  }

}
