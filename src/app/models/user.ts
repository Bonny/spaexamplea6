export class User {
    userid: number;
    username: string;
    roles: Array<string>;

    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}