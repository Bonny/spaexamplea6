import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { LoggerService } from './logger.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpRestInterceptorService {

  constructor(private logger: LoggerService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.logger.debug("HttpRestInterceptor.intercept");
    var headers = {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
      'Access-Control-Allow-Headers': 'origin, content-type, accept, authorization'
    };
    let currentUser = JSON.parse(localStorage.getItem(environment.currentUser));
    if (currentUser && currentUser.token) {
      headers['Authorization'] = 'Bearer ' + currentUser.token;
    }
    return next.handle(request.clone({ setHeaders: headers }));
  }
}
