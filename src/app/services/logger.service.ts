import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor() { }

  private log(level: string, msg: string): void {
    console.log("[" + level + "] " + msg);
  }

  info(msg: string): void {
    this.log("INFO", msg);
  }

  warn(msg: string): void {
    this.log("WARN", msg);
  }

  debug(msg: string): void {
    this.log("DEBUG", msg);
  }

  error(msg: string): void {
    this.log("ERROR", msg);
  }
  
}
