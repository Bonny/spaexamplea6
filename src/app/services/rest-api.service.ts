import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { LoggerService } from './logger.service';
import { User } from '../models/user';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private user: User;

  constructor(
    private http: HttpClient,
    private logger: LoggerService
  ) { }

  private urlBase64Decode(str): string {
    var output = str.replace('-', '+').replace('_', '/');
    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw 'Illegal base64url string!';
    }
    return window.atob(output);
  }

  private setUser(): void {
    var token = localStorage.getItem(environment.currentUser);
    var input = {};
    if (typeof token !== 'undefined') {
      var encoded = token.split('.')[1];
      input = JSON.parse(this.urlBase64Decode(encoded));
      this.user = new User().deserialize(JSON.parse(input["user"]));
    }
  }

  getUser(): User {
    return this.user;
  }

  login(username: string, password: string) {
    return this.http.post<any>(API_URL + '/v1/authentication', { username: username, password: password })
      .pipe(map((res: any) => {
        this.logger.debug("login successful if there's a jwt token in the response");
        if (res && res.token) {
          this.logger.debug("store username and jwt token in local storage to keep user logged in between page refreshes");
          this.logger.debug(res.token);
          localStorage.setItem(environment.currentUser, JSON.stringify({ username, token: res.token }));
          this.setUser();
        }
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem(environment.currentUser);
  }
}